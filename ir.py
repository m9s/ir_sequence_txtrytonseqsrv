#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import datetime
import xmlrpclib

from trytond.model import ModelView, ModelSQL
from trytond.transaction import Transaction
from trytond.config import CONFIG


SEQUENCE_SERVICE_URL = CONFIG.get('sequence_service_url', False)
SEQUENCE_SERVICE_REQUESTER = CONFIG.get(
    'sequence_service_requester', 'trytond')


class Sequence(ModelSQL, ModelView):
    _name = 'ir.sequence'

    def __init__(self):
        super(Sequence, self).__init__()
        self._error_messages.update({
            'no_seq_service': 'Unable to get sequence from service!',
            })

    def get_id(self, domain):
        if SEQUENCE_SERVICE_URL:
            return self._get_id_sequence_server(domain)

        return super(Sequence, self).get_id(domain)

    def _get_id_sequence_server(self, domain):
        '''
        Returns a sequence number for the domain from the sequence server.

        :param domain: a domain or a sequence id
        :return: the sequence number
        '''
        _datetime = None
        date = Transaction().context.get('date')
        if date:
            _datetime = datetime.datetime.combine(date, datetime.time())

        server = xmlrpclib.Server(SEQUENCE_SERVICE_URL)
        if isinstance(domain, (int, long)):
            return server.tryton.requestNumber(domain, self._strict,
                SEQUENCE_SERVICE_REQUESTER, _datetime)

        with Transaction().set_context(user=False):
            with Transaction().set_user(0):
                sequence_ids = self.search(domain, limit=1)
            if sequence_ids:
                return server.tryton.requestNumber(sequence_ids[0],
                    self._strict, SEQUENCE_SERVICE_REQUESTER, _datetime)

        self.raise_user_error('no_seq_service')

Sequence()


class SequenceStrict(Sequence):
    _name = 'ir.sequence.strict'

    def get_id(self, domain):
        if not SEQUENCE_SERVICE_URL:
            Transaction().cursor.lock(self._table)

        return super(SequenceStrict, self).get_id(domain)

SequenceStrict()
