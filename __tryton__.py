# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'IR Sequence Txtrytonseqsrv',
    'name_de_DE': 'Interne Administration Nummernkreis txtrytonseqsrv',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Provides functionality to retrieve all sequences from
        an external txtrytonseqsrv service.
    ''',
    'description_de_DE': '''Stellt Funktionalität zur Verfügung, um
        alle Nummernkreise von einem externen txtrytonseqsrv Service
        bedienen zu können.
    ''',
    'depends': [
        'ir',
        'res',
        'ir_sequence_log',
    ],
    'xml': [
    ],
    'translation': [
        # 'locale/de_DE.po',
    ],
}
