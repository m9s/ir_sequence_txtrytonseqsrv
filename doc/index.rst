IR Sequence Txtrytonseqsrv Module
#################################
This module provides the functionality to retrieve all sequences from a
*txtrytonseqsrv* service, which is distributed as a separate twisted
application.
Every time the next number of a sequence is requested, the call is delegated
to and answered by the external service.


Configuration options
*********************
The connection to the external service and the name of the requesting
application are configured in ``trytond.conf``:

sequence_service_url:
    The connection URL for the external service. When this option is set, all
    requests for next numbers of any sequence are delegated to the external
    service. The default value is "False". Example::

        sequence_service_url = http://USER:PASSWORD@localhost:8060


sequence_service_requester:
    Name serving to identify the requester. This name is shown in model
    *ir.sequence.log*, field *Requester* log entries,

        * when trytond requests a sequence number from the service, and
        * model *ir.sequence*, field *Log* is checked.

    The default value is "trytond".
